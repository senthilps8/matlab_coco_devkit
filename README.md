Matlab MSCOCO[1] Devkit
=======================

Installation:
------------
* Run setup.m. It downloads the necessary files (including Annotations)
* Modify cocoInit.m
	+ change files_path to the path of the files folder downloaded
	+ change img_folder_path to the path of the COCO image folder


Usage:
-----
Each file has a short instruction in the beginning. Here is a summary:

* __get_image_ids(image_set)__ gives a list of image IDs for that set.
* __img_path_from_id(id,image_set)__ gives the path to the image
* __load_seg_annotation_from_id(id)__ loads an annotation structure (format explained below)
* __generate_GT_mask_from_ann(annotation_structure)__ creates GT masks for the annotations
	+ Can be used to generate Pascal VOC style object and class segmentations (options explained in the file)
* __cat_id_to_classname(id)__ gives the name of the class from the COCO category ID.



Annotation Structure:
--------------------
The load_seg_annotation_from_id() function creates an annotation structure with the following format:

* annotation fields:
	- dim (dimensions of image)
	- instances (cell array of instance structures)
	- image_id

* annotation.instances structure fields:
	- inst_id (instance id of object)
	- cat_id (category id of object)
	- num_poly (number of polygons in the segmentation)
	- polygons (cell array of 2XN polygons - N vertices in [x;y] format)
	- area (area of object)
	- bbox (bounding box of object)



References:
----------
1. Tsung-Yi Lin, Michael Maire, Serge Belongie, James Hays, Pietro Perona, Deva Ramanan, Piotr Dollár, C. Lawrence Zitnick: Microsoft COCO: Common Objects in Context.




To Add:
------
1. Bounding box extraction from Annotation
2. Sentence annotations
*****
