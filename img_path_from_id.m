% Generates path to the image file from image ID
% Input:
%       id: Image ID
%	imgset: Image Set name (train2014 or val2014)
%
% Output:
%       path: list of paths
%
%
% Author: Senthil Purushwalkam
% For contributing or reporting bugs, email me at senthilps8@gmail.com or senthil@vt.edu
% Updates can be found at https://filebox.ece.vt.edu/~senthil/code.html

function path = img_path_from_id(id, imgset)

	cocoInit;
	if(isstr(id))
		id=str2num(id);
	elseif(iscellstr(id))
		tmp_ids=zeros(length(id));
		for i=1:length(id)
			tmp_ids=str2num(id{i});
		end
		id=tmp_ids;
	end

	path={};
	for i=1:length(id)
		path{i}=[img_folder_path imgset '/COCO_' imgset '_' num2str(id(i),'%012d') '.jpg'];
	end
