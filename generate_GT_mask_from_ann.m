% Generates GT masks from annotation stuctures
% Input:
%       ann: annotation structure from load_seg_annotation_from_id
%	type: Pascal VOC style masks
%		'class' - One mask with category_id labelled for each pixel
%	      	'object' - One mask with each object labelled with a unique number from 1:num_objects
%		no input or 'none' - One binary mask per object
%	class: Generate annotations for only category id = class	
%
% Output:
%       masks: 2D mask for 'class' and 'object' type and 3D mask for 'none' type
%	cat_ids: category id for each mask (useful with 'none' type)
%
%
% Author: Senthil Purushwalkam
% For contributing or reporting bugs, email me at senthilps8@gmail.com or senthil@vt.edu
% Updates can be found at https://filebox.ece.vt.edu/~senthil/code.html

function [masks cat_ids]=generate_GT_mask_from_ann(ann,type,class)

	if nargin<2
		type='none';
		class=0;
	elseif nargin<3
		class=0;
	end

	%Need to check this for class~=0 input
	if(class~=0)
		rm=[];
		for i=1:length(ann.instances)
			if(ann.instances{i}.cat_id~=class)
				rm=[rm; i];
			end
		end
		ann.instances(rm)=[];
	end



	dim=[ann.dim(2) ann.dim(1)];
	cat_ids=zeros(length(ann.instances),1);
	seg_masks=zeros(dim(1),dim(2),length(ann.instances));
	for i=1:length(ann.instances)
		bw=zeros(dim);
		if(ann.instances{i}.area<5)  %because poly2mask doesn't work on a few very small annotations
			%Need to check this..
			for j=1:length(ann.instances{i}.polygons)
				bw(round(ann.instances{i}.polygons{j}(2,:)'),round(ann.instances{i}.polygons{j}(1,:)'))=1;
			end
		else
			for j=1:length(ann.instances{i}.polygons)
				bw_tmp=poly2mask(ann.instances{i}.polygons{j}(1,:), ann.instances{i}.polygons{j}(2,:), dim(1), dim(2));
				bw=bw_tmp|bw;
			end
		end
		cat_ids(i)=ann.instances{i}.cat_id;
		seg_masks(:,:,i)=bw;
	end
	masks=seg_masks;


	if(strcmp(type, 'class'))
		masks=zeros(dim);
		for i=1:length(ann.instances)
			masks(find(seg_masks(:,:,i)))=cat_ids(i);
		end
	elseif(strcmp(type,'object'))
		masks=zeros(dim);
		for i=1:length(ann.instances)
			masks(find(seg_masks(:,:,i)))=i;
		end
	end
