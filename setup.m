% Matlab COCO Devkit Setup
%
% Author: Senthil Purushwalkam
% For contributing or reporting bugs, email me at senthilps8@gmail.com or senthil@vt.edu
% Updates can be found at https://filebox.ece.vt.edu/~senthil/code.html
% 

untar('https://filebox.ece.vt.edu/~senthil/shared_files/matlab_coco_devkit_files.tar.gz');
display('Modify cocoInit.m now and set files_path to this folder..')
