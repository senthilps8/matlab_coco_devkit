% Sets up the paths required for the Matlab COCO devkit
%
% Author: Senthil Purushwalkam
% For contributing or reporting bugs, email me at senthilps8@gmail.com or senthil@vt.edu
% Updates can be found at https://filebox.ece.vt.edu/~senthil/code.html

%CHANGE THESE according to your setup
files_path='~/newreps/matlab_coco_devkit/files'; %Path to folder downloaded by setup.m
img_folder_path='~/datasets/coco/images/'; %Path to folder countating coco Images (with subdirectores train2014 and val2014)

anno_path=[files_path '/Annotations/%d.txt'];
ids_path=[files_path '/%s_ids.mat'];
class_hash_path=[files_path '/class_hash.mat'];
