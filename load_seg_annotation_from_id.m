% Loads segmentation annotations for an input Image ID.
% Input:
%	id: Image ID
%
% Output:
%	ann: annotation structure containing all segmentations
%
%
% Author: Senthil Purushwalkam
% For contributing or reporting bugs, email me at senthilps8@gmail.com or senthil@vt.edu
% Updates can be found at https://filebox.ece.vt.edu/~senthil/code.html

function ann = load_seg_annotation_from_id(id)
	if(isstr(id))
		id=str2num(id);
	end
	cocoInit;

	f=fopen(sprintf(anno_path,id),'r');
	if(f==-1)
		display('Warning: Bad Annotation path/Image ID');
	end


	ann=struct;
	ann.dim=[];
	ann.instances={};
	ann.image_id=id;
	
	%Read Dimensions
	line=fgets(f);
	ann.dim=str2num(line);

	%Read Instance ID
	line=fgets(f);

	while(isstr(line))
		inst=struct;
		inst.inst_id=str2num(line);

		%Read Category ID and Number of polygons
		line=fgets(f);
		tmp=str2num(line);
		inst.cat_id=tmp(1);
		inst.num_poly=tmp(2);
		inst.polygons={};

		for i=1:inst.num_poly
			line=fgets(f);
			tmp=str2num(line);
			inst.polygons{i}=[tmp(1:2:end-1); tmp(2:2:end)];
		end

		line=fgets(f);
		inst.area=str2num(line);
		line=fgets(f);
		inst.bbox=str2num(line);
		
		ann.instances=[ann.instances; inst];

		line=fgets(f);
	end
	fclose(f);

        if(length(ann.instances)==0)
		display('Warning: Zero Annotations avaliable for this image');
	end
