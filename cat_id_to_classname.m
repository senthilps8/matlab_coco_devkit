% String Category Name from COCO Category ID (1-90)
% Input:
%       id: Category ID or list of IDs
%
% Output:
%       name: list of category names
%
%
% Author: Senthil Purushwalkam
% For contributing or reporting bugs, email me at senthilps8@gmail.com or senthil@vt.edu
% Updates can be found at https://filebox.ece.vt.edu/~senthil/code.html

function name = cat_id_to_classname(id)
	cocoInit;
	load(class_hash_path);

	name={};
	for i=1:length(id)
		name{i}=class_hash(id(i));
	end
