% Get List of All Image IDs for an image set
% Input:
%       imgset: Image Set name (train2014 or val2014)
%
% Output:
%       ids: int32 array of IDs
%
%
% Author: Senthil Purushwalkam
% For contributing or reporting bugs, email me at senthilps8@gmail.com or senthil@vt.edu
% Updates can be found at https://filebox.ece.vt.edu/~senthil/code.html

function ids = get_image_ids(imgset)

	cocoInit;
	path=sprintf(ids_path,imgset);
	ids=load(path);
	ids=ids.ids;
